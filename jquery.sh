#! /bin/bash
#@ jquery.sh

wget=$(which wget) || exit $?

wget -O jquery.js http://code.jquery.com/jquery.min.js &&
wget -O jquery.map http://code.jquery.com/jquery.min.map &&
sed -i "s/jquery\.min\.map/jquery\.map/" jquery.js
