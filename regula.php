#! /usr/bin/env php
<?php
// Copyright © 2015 Frère Jérôme, Abbaye de Maredsous
// Œuvre protégée par une licence de libre diffusion à vocation non-commerciale (cf. « LICENSE »)

if (file_exists('regula.xml') && is_dir('regula')) {
  if ($reg = simplexml_load_file('regula.xml')) {
    for ($c = 0; $c < 73; $c++) {
      if ($cha = json_encode($reg->c[$c], JSON_UNESCAPED_UNICODE)) {
        $out = "regula/$c.json";
        if ((! file_exists($out)) || (md5($cha) != md5_file($out))) {
          file_put_contents($out, $cha);
          echo $out . PHP_EOL;
        }
      }
    }
  }
}
