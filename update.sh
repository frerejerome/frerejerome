#! /bin/bash
#@ update.sh

git=$(which git) || exit $?

if [ -d .git ]; then
  $git reset --quiet --hard &&
  $git clean --force &&
  $git pull $@
else
  $git clone --single-branch --depth 1 $@ \
    git://github.com/frerejerome/frerejerome.github.io .
fi
