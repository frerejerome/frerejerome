#! /bin/bash
#@ commit.sh

git=$(which git) || exit $?

date=$(date +%F)
change=$(grep ^$date CHANGES | head -n1 | cut -d' ' -f2-)
update=$(stat -c %y index.html | cut -d' ' -f1)
lcdate=$(date +%-d/%-m/%Y -d$date)

sed -i "s/\(datetime=\"\).*\"/\1$update\"/;s%>.*\(<\/time>\)%>$lcdate\1%" index.html &&
sed -r "s/%index%/$update/" sitemap.tmp >sitemap.xml &&
git commit -a -m"$change" &&
git push $@
