![La Règle de saint Benoît](http://la.regle.org/regle.png)

## La Règle de saint Benoît

Traduite en français par Dom [Germain Morin](https://fr.wikipedia.org/wiki/Germain_Morin_%28bénédictin%29)
de l'[Abbaye de Maredsous](http://maredsous.com)
**(1944)**

Revue et annotée à partir de la traduction de Dom Philibert Schmitz
de la même Abbaye
**(2015)**

Copyright © 2015 [Frère Jérôme](https://github.com/frerejerome), [Abbaye de Maredsous](http://maredsous.com)

Œuvre protégée par une [licence de libre diffusion Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)
à vocation non-commerciale
